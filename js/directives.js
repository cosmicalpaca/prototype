'use strict';

angular.module('prototype.directives', []).
    directive('ampFade', function() {
      return {
          restrict: 'A',
          link: function (scope, element, attrs) {
              var length = attrs.ampFadeLength || 400;
              element.css({
                  '-webkit-transition': 'opacity ' + length/1000 + 's',
                  'opacity': 0,
                  'display': 'none'
              });

              scope.$watch(attrs.ampFade, function (value) {
                if (value) {
                    element
                        .css({
                            'display': 'block'
                        })
                    setTimeout(function(){
                        element
                            .css({
                                'opacity': '1',
                                'display': 'block'
                            });
                    }, 0);
                }
                else if (value === false && element.css('display') === 'block') {
                    element
                        .css({
                            'opacity': 0
                        })
                        .bind('webkitTransitionEnd', function () {
                            $(this).css({'display': 'none'});
                            $(this).unbind('webkitTransitionEnd');
                        });
                }
              });
          }
      }
    }).
  directive('repeat', ['$compile', function($compile) {

    return{
        restrict: 'E',
//        link: function(scope, elm, attrs) {
//            var item,
//                parent = ($(elm).parent());
//
//            angular.forEach(scope[attrs.repeat], function(value, index) {
//                item = $("<div></div>").addClass('item').addClass('i'+ (index+1));
//                item.append( $("<div></div>").addClass('circle').addClass(value.color+"bg"));
//                item.append( $("<div></div>").addClass('title medium bold').text(value.title) );
//                item.data({index: index});
//
//                parent.append(item);
//                $(elm).remove();
//            });
//        },
        link: function(scope, element, attrs) {
            var html = '';
            scope.hi = function() { console.log("hi"); }
            angular.forEach(scope[attrs.repeat], function(value, index) {
                var i = index + 1;
                console.log(scope);
                html += "" +
                    "<div data-index='"+index+"' class='item i"+i+"'>" +
                    "<div class='circle "+value.color+"bg'></div>" +
                    "<div class='title medium bold'>"+value.title+"</div>" +
                    "</div>";

            });
            element[0].outerHTML = html;
            $compile(element.contents())(scope);
        }
    }
  }]);
