'use strict';
// Declare app level module which depends on filters, and services

angular.module('prototype', ['prototype.filters', 'prototype.services', 'prototype.directives', 'prototype.controllers']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/lessonmap',
        {templateUrl: 'partials/lessonmap.html', controller: 'LessonMap'});
    $routeProvider.otherwise({redirectTo: '/lessonmap'});
  }]);
