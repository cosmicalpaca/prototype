'use strict';

angular.module('prototype.controllers', []).
  controller('LessonMap', ['$scope', function($scope) {
        $scope.currentPane = '';
        $scope.startRevealed = false;
        $scope.startClicked = false;
        $scope.paneOpened = false;
        $scope.dimmed = false;

        $scope.stubClicked = function () {
            $scope.dimmed = !$scope.dimmed;
        }
        $scope.dimmerClicked = function () {
            $scope.dimmed = false;
            $scope.startRevealed = false;
        }
        $scope.paneCloseClicked = function () {
            $('.selected').click();
        }

        $scope.items = [
            {
                title: "Vocab Warm Up",
                color: "blue",
                time: 7
            },
            {
                title: "Define Showing",
                color: "green",
                time: 2
            },
            {
                title: "Examples of Showing",
                color: "red",
                time: 8
            },
            {
                title: "Picture Precise Locations",
                color: "yellow",
                time: 5
            },
            {
                title: "Writing Time Sharing",
                color: "blue",
                time: 23
            },
            {
                title: "Define Showing",
                color: "red",
                time: 4
            },
            {
                title: "Examples of showing",
                color: "green",
                time: 2
            }
        ]

        $scope.$on('$routeChangeSuccess', function () {
            var wrapper = $('.wrapper'),
                content = $('.content'),
                items = $('.item'),
                pane = $('.pane'),
                well = $('.well'),
                starter = $('.starter'),
                selected;

            items.on('click', function () {
                var clicked = $(this);

                // Switching to another
                if (selected && !$(this).hasClass('selected')) {
                    selected.removeClass('selected');
                    selected.anim('putBelow');

                    selected = $(this);

                    pane.anim('paneDownUp');
                    selected.anim('putAbove', transition_length);
                    selected.addClass('selected');
                    $scope.currentPane = $scope.items[clicked.data('index')];
                    updateWell(clicked);

                    $scope.$apply();
                } else
                // Clicking the same
                if (selected && $(this).hasClass('selected')) {
                    items.removeClass('shifted');
                    selected.removeClass('selected');
                    pane.removeClass('up');
                    $scope.paneOpened = false;
                    wrapper.removeClass('dimmed');
                    selected = null;
                    $scope.$apply();
                // Clicking a new one
                } else {
                    selected = $(this);
                    items.addClass('shifted');
                    selected.addClass('selected');
                    pane.addClass('up');
                    $scope.paneOpened = true;
                    wrapper.addClass('dimmed');

                    $scope.currentPane = $scope.items[clicked.data('index')];
                    updateWell(clicked);

                    $scope.$apply();
                }


                function updateWell(clicked) {
                    well.css({left: clicked.css('left')});
                }

            });


//            items.on('click', function () {
//                var clicked = $(this);
//
//                // Switching to another
//                if (selected && !$(this).hasClass('selected')) {
//                    selected.anim('putBelow', 500).removeClass('selected');
//
//                    selected = $(this);
//
//                    selected.anim('putAbove', 500).addClass('selected');
//
//                    setTimeout(function(){
//                        $scope.currentPane = $scope.items[clicked.data('index')];
//                        $scope.$apply();
//                    }, transition_length/2);
//
//                    $('.pane .content').anim('paneSwap', transition_length);
//                    updateWell(clicked);
//
//                } else
//                // Clicking the same
//                if (selected && $(this).hasClass('selected')) {
//                    items.toggleClass('shifted');
//                    selected.toggleClass('selected');
//                    pane.toggleClass('up');
//                    wrapper.toggleClass('dimmed');
//
//                    selected = null;
//                    well.removeClass('shown');
//                // Clicking a new one
//                } else {
//                    selected = $(this);
//
//                    items.toggleClass('shifted');
//                    selected.toggleClass('selected');
//                    pane.toggleClass('up');
//                    wrapper.toggleClass('dimmed');
//
//                    $scope.currentPane = $scope.items[clicked.data('index')];
//                    $scope.$apply();
//
//                    updateWell(clicked);
//                }
//
//                function updateWell(clicked) {
//                    if (!well.hasClass('shown')) {
//                        well.css({left: clicked.css('left')});
//                        well.addClass('shown')
//                    } else {
//                        well.anim('toggleWell', transition_length, function () {
//                            well.css({left: clicked.css('left')});
//                        });
//                    }
//                }
//
//            });
        });

    }])



  .controller('MyCtrl2', [function() {

  }]);