var transition_length = 400;

$.fn.anim = function(animation, duration, fn) {
    if (!duration) duration = transition_length;
    return this.each(function(){
        $(this).css({'-webkit-animation': animation+" "+(duration/1000)+"s"}).bind('webkitAnimationEnd', function(){
            $(this).css({'-webkit-animation':''});
            if (fn) fn();
        });
//        if (fn) {
//            setTimeout(fn, duration/2);
//        }
    });
}

$('.wrapper').ontouchmove = function(event){
    event.preventDefault();
}


//$(document).ready(function () {
//    $(window)
//        .bind('orientationchange', function(){
//            console.log(window.orientation);
//            if (window.orientation == 90) {
//                $(document.body).css("-webkit-transform-origin", "")
//                    .css({"-webkit-transform":"", "height":"auto", "width":"auto"});
//                console.log("0deg")
//            } else
//            if (window.orientation == -90) {
//                $(document.body).css("-webkit-transform-origin", "")
//                    .css({"-webkit-transform":"", "height":"auto", "width":"auto"});
//                console.log("0deg")
//            } else {
//                $(document.body).css("-webkit-transform-origin", "490px 490px")
//                    .css({"-webkit-transform":"rotate(90deg)", "height":"1024px", "width":"1184px"});
//                console.log("90deg")
//            }
//        })
//        .trigger('orientationchange');
//});